# SER 502 - Lab 1 - Rust Language
### Zahra AL-Kaf, Christian Murphy and Saurabh Tandan
************************************************************
##Project 1:[Team 15 Rust Video](https://www.youtube.com/watch?v=gfEKiRSBQ4U)

************************************************************

##How to install Rust:

####To install Rust on [Mac](https://static.rust-lang.org/dist/rust-nightly-i686-apple-darwin.pkg "Mac")

####To install Rust on [Windows](https://static.rust-lang.org/dist/rust-nightly-i686-pc-windows-gnu.exe "Windows")
				 
Click [here](http://www.rust-lang.org/install.html) for installation instructions. 
			
*************************************************************
##You might also want to try this [Online Compiler](https://play.rust-lang.org/ "Title")