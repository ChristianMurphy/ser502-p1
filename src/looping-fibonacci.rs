fn main() {
	let x = 4;
	let f = fibo(x);
	println!("Fibo({}) is {}", x, f);
}

fn fibo (x: i32) -> i32 {
	println!("x is {}", x);
	let mut a = 0;
	let mut b = 1;
	let mut count = 0;
	loop {
		if count >= x {
			break;
		}
		let temp = a;
		a = b;
		b = temp +b;
		count = count + 1;
		println!("b is {}", b);
	}
	return b;
}
