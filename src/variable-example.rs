fn main() {
	let hi = "hi"; //inmuttable variable
	let mut count = 0; 

	while count < 10 {
		println!("{}", hi);
		println!("{} is {}", hi, count);
		count += 1;
	}
}
