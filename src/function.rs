fn main() {
	println!("Hello functions");
	let x = square(10);
	println!("{}", x);
}

fn square (x: i32) -> i32 {
	x * x
}
