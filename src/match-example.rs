fn main() {
	let my_number = 10;

	match my_number {
	0		=> println!("zero"),
	1 | 2	=> println!("one oe two"),
	3...10	=> println!("three to 10"),
	_		=> println!("something else")

	}

	let (a,b) = ret_test_tuple();
	println!("{}", my_number);
	println!("{}", a);
	println!("{}", b);
}

fn ret_test_tuple() -> (i16, i16) {
	return (10, 20);
}
