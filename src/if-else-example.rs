// if statement as an expression


fn main() {
    let x = 5;
    let var = if x == 5 {
                "five"
          }
          else if x == 6 {
                "six"
          }
          else {
                "neither"
          };
    println!("var = {}", var);
}